<?php
session_start();
require "./User.php";
require "./UserModel.php";
$post = $_POST;
$mysql = new mysqli('localhost', 'root', 'password', 'intern');

if ($mysql->connect_errno !== 0) {
	echo "Failed to connect to database" . $mysql->connect_error;
}

function validate(array $post)
{
	$errors = [];

	if (!strlen($post['name']) > 0) {
		$errors['name'] = "Please enter name";
	}

	if (!strlen($post['email']) > 0) {
		$errors['email'] = "Please enter email";
	}

	if (!strlen($post['password']) > 0) {
		$errors['password'] = "Please enter password";
	}

	return $errors;
}

function displayErrors(array $errors)
{
	$error = '';
	foreach ($errors as $err) {
		$error .= $err . '<br>';
	};

	echo '<div class="alert alert-danger mt-3">' . $error . '</div>';
}




if (isset($post['submit'])) {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$password = $_POST['password'];

	$errors = validate($post);


	if (!$errors) {
		$hash = password_hash($password, PASSWORD_DEFAULT);
		$user = new User($name, $email, $hash);
		$userModel = new UserModel($mysql);
		if ($userModel->saveUser($user)) {
			$msg = "User created successfully";
		} else {
			$errors = [];
			$errors['create'] = "Failed to create user";
		}
	}
}
?>

<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

	<title>Hello, world!</title>
</head>

<body>

	<div class="container">
		<div class="row mt-5">
			<div class="col-lg-8 offset-lg-2">
				<div class="card">
					<div class="card-body">
						<form action="http://localhost:4000/http-request" method="POST">
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" class="form-control">
							</div>
							<div class="form-group">
								<label>Email</label>
								<input type="text" name="email" class="form-control">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control">
							</div>
							<div class="form-group mt-2">
								<button type="submit" name="submit" class="btn btn-primary">Submit</button>

							</div>
						</form>

						<?php if (isset($errors) && $errors) : ?>
							<?php displayErrors($errors); ?>
						<?php endif ?>

						<?php if (isset($msg)) : ?>
							<div class="alert alert-success"><?php echo $msg ?></div>
						<?php endif ?>


						<?php if (isset($_SESSION['loginMsg'])) : ?>
							<div class="alert alert-danger">
								<?php echo $_SESSION['loginMsg'];
								unset($_SESSION['loginMsg']) ?>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>
