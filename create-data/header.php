<?php
session_start();
if (!isset($_SESSION['email']) || !isset($_SESSION['isLoggedIn'])) {
	header("Location: ./index.php");
	$_SESSION['loginMsg'] = "You must login first";
	exit();
}
