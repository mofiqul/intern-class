<?php
$post = $_POST;
if (isset($post['submit'])) {
	$email = $_POST['email'];
	$password = $_POST['password'];

	$issError = false;
	if (!strlen($email) > 0) {
		$issError = true;
		$msgEmail = "Please enter email";
	}

	if (!strlen($password) > 0) {
		$issError = true;
		$msgPass = "Please enter password";
	}
}
?>


<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

	<title>Hello, world!</title>
</head>

<body>

	<div class="container">

		<form action="http://localhost:4000/http-request" method="POST">
			<div class="form-group">
				<label>Email</label>
				<input type="email" name="email" class="form-control">
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" name="password" class="form-control">
			</div>
			<div class="form-group">
				<button type="submit" name="submit" class="btn btn-primary">Submit</button>

			</div>
		</form>

		<?php if ($issError) : ?>
			<div class="alert alert-danger mt-2">
				<?php
				if (strlen($msgEmail) > 0) {
					echo $msgEmail . '<br>';
				}
				if (strlen($msgPass) > 0) {
					echo $msgPass;
				}
				?>
			</div>
		<?php endif ?>
	</div>


	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>
