<?php

class User
{
	public $id = 0;

	public $name;

	public $email;

	public $password;

	public static function create(string $name, string $email, string $password)
	{
		$self = new static;
		$this->name = $name;
		$this->email = $email;
		$this->password = $password;
		return $self;
	}

	public static function createFromDbRow(int $id, string $name, string $email, string $password)
	{
		$self = new static;
		$self->id = $id;
		$self->name = $name;
		$self->email = $email;
		$self->password = $password;

		return  $self;
	}
}

