<?php
include "./header.php";
require "./database.php";
$mysql = Database::getInstance();

$sql = "SELECT id, name, email FROM users";
$res = $mysql->query($sql);
$users = [];

if($res->num_rows) {
	while($row = $res->fetch_object()){
		$users[] = $row;
	}
}
?>



<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-bordered">
				<thead>
					<th>Id</th>
					<th>Name</th>
					<th>Email</th>
				</thead>
				<tbody>
				<?php foreach($users as $user):?>
				<tr>
					<td><?php echo $user->id?></td>
					<td><?php echo $user->name?></td>
					<td><?php echo $user->email?></td>
				</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		</div>
	</div>
</div>




