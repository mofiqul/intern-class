<?php
# Singleton pattern 
class Database 
{
	public static $instance = null;

	public static function getInstance(){
		if (!self::$instance){
			self::$instance = new mysqli('localhost', 'root', 'password', 'intern');
		}

		return self::$instance;
	}

	private function __construct(){}

	private function __clone(){}
}

