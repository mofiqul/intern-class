<?php
class UserModel
{
	private $connection;

	public function __construct(mysqli $connection)
	{
		$this->connection = $connection;
	}

	/**
 	* @param User
 	*/
	public function saveUser(User $user)
	{
		$query = "INSERT INTO users (name, email, password) VALUES('$user->name', '$user->email', '$user->password')";
		return $this->connection->query($query);
	}

	/**
	* @return User[] | []
	*/
	public function getUsers()
	{
		$sql = "SELECT id, name, email, password FROM users";
		$res = $this->connection->query($sql);
		$users = [];
		
		if($res->num_rows){
			while($row = $res->fetch_object()){
				$users[] = User::createFromDbRow($row->id, $row->name, $row->email, $row->password); 
			}
		}

		return $users;

	}
}
