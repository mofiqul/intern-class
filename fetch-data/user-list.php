<?php
include "./header.php";
require "./database.php";
require "./User.php";
require "./UserModel.php";

$mysql = Database::getInstance();
$userModel = new UserModel($mysql);
$users = $userModel->getUsers();
?>



<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-bordered">
				<thead>
					<th>Id</th>
					<th>Name</th>
					<th>Email</th>
				</thead>
				<tbody>
				<?php foreach($users as $user):?>
				<tr>
					<td><?php echo $user->id?></td>
					<td><?php echo $user->name?></td>
					<td><?php echo $user->email?></td>
				</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		</div>
	</div>
</div>




