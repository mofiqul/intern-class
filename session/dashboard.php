<?php
session_start();
if (!isset($_SESSION['email']) || !isset($_SESSION['isLoggedIn'])) {
	header("Location: ./index.php");
	$_SESSION['loginMsg'] = "You must login first";
	exit();
}
?>

<h1>Hello: <?php echo $_SESSION['email'] ?></h1>
<a href="./logout.php">Logout</a>
